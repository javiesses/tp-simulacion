var readline = require('readline');
	helpers = require('./helpers')();
    gamma = require('gamma');

//inicializacion todas las variables y constantes. Todos los tiempos se expresan en minutos
var 
	t = 0, 			//tiempo
	CPTC = 0, 		//cantidad de puestos tramites cortos
	CPTL = 0, 		//cantidad de puestos tramites largos
	tpll = 0, 		//tiempo proxima llegada
	tpstl = [],		//tiempo proxima salida tramite largo
	tpstc = [],		//tiempo proxima salida tramite corto
	nt = 0,			//numero total de personas que pasaron por el sistema
	nstl = 0,		//cantidad de personas en sistema tramite largo
	nstc = 0,		//cantidad de personas en sistema tramite corto
	nsft = 0,		//cantidad de personas en sistema fuera de turno
	i = 0,			//auxiliar para tramites largos	
	j = 0,			//auxiliar para tramites cortos
	itotl = [],		//inicio tiempo ocioso por puesto tramite largo
	itotc = [],		//inicio tiempo ocioso por puesto tramite corto
	sta = 0,		//sumatoria tiempo de atencion
	ss = 0,			//sumatoria salidas
	sll = 0,		//sumatoria llegadas
	stotl = [],		//sumatoria tiempo ocioso tramites largos
	stotc = [],		//sumatoria tiempo ocioso tramites cortos
	pte = 0,		//promedio tiempo de espera
	ptotl = [],		//promedio tiempo ocioso tramite largo
	ptotc = []		//promedio tiempo ocioso tramite corto
	ntatl = 0,		//numero total de gente arrepentida tramite largo
	ntatc = 0,		//numero total de gente arrepentida tramite corto
	ntaft = 0,		//numero total de gente arrepentida fuera de turno
	nta = 0,		//numero total de gente arrepentida
	patl = 0,		//porcentaje de arrepentidos tramite largo
	patc = 0,		//porcentaje de arrepentidos tramite corto
	paft = 0;		//porcentaje de arrepentidos fuera de turno

const
	HV = Number.MAX_VALUE,				//high value
	CANTIDAD_SIMULACIONES = 10000,
	TF = 800; 							//tiempo final

var rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout
});

console.log('Comenzando con el TP Final de Simulación...');


//ingreso variables de control
//ingreso CPTL
rl.question('Ingrese la cantidad de puestos de atención para trámites largos (CPTL): ', (cantTL) => {
	CPTL = parseInt(cantTL);
	//seteo HV para todas las salidas de los puestos de atencion de tramites largos y lleno array con 0s
	for(var h = 0; h < CPTL; h++) {
		tpstl.push(HV);
		stotl.push(0);
		ptotl.push(0);
		itotl.push(0);
	}

	//ingreso CPTC
	rl.question('Ingrese la cantidad de puestos de atención para trámites cortos (CPTC): ', (cantTC) => {
		CPTC = parseInt(cantTC);
		//seteo HV para todas las salidas de los puestos de atencion de tramites largos y lleno array con 0s
		for(var h = 0; h < CPTC; h++) {
			tpstc.push(HV);
			stotc.push(0);
			ptotc.push(0);
			itotc.push(0);
		}
	
		for( var c = 0; c < CANTIDAD_SIMULACIONES; c++) {

			console.log('Corriendo simulacion ', c + 1);
			inicializarVariables();

			//mientras t <= tf o hayan personas en el sistema, se corre la simulacion
			while (t <= TF || nstc > 0 || nstl > 0) {
				i = helpers.buscarMenor(tpstl);
				j = helpers.buscarMenor(tpstc);

				if (tpstl[i] <= tpstc[j]){

					if (tpstl[i] < tpll)
						procesarSalidaTramiteLargo();					
					else
						procesarLlegada();

				} else {

					if (tpll <= tpstc[j])
						procesarLlegada();
					else 
						procesarSalidaTramiteCorto();
					
				}

				//si ya se paso el TF, pero quedan personas en el sistema => seteo HV para tpll
				if(t > TF && (nstc > 0 || nstl > 0))
					tpll = HV;
			}

			//calculo de resultados
			//promedio tiempo de espera
			pte += (ss - sll - sta) / nt;
			
			//arrepentidos
			nta += ntatc + ntatl + ntaft;
			patc += (ntatc * 100) / nta;
			patl += (ntatl * 100) / nta;
			paft += (ntaft * 100) / nta;

			//tiempo ocioso
			for(var h = 0; h < CPTL; h++) 
				ptotl[h] += (stotl[h] * 100) / t;

			for(var h = 0; h < CPTC; h++) 
				ptotc[h] += (stotc[h] * 100) / t;

		}

		helpers.mostrarResultados(ptotl, ptotc, pte, nta, patc, patl, paft,CANTIDAD_SIMULACIONES, CPTL, CPTC);

		console.log('Fin de la Simulación');
		process.exit();
	});
});

function procesarSalidaTramiteCorto(){
	//actualizo t con el valor de la proxima salida y resto una persona del sistema tramite corto
	t = tpstc[j];
	nstc--;

	//si hay gente en la cola fuera de turno (prioridad), se la atiende y se la suma al nstc
	if (nsft >= 1){
		nsft--;
		nstc++;

		//obtengo tatc y lo sumo para tpstc y la sumatoria de ta
		var taft = helpers.getTaft();
		tpstc[j] = t + taft;
		sta += taft;
	} else {
		//no hay gente con prioridad, pregunto si hay gente en tramites cortos esperando
		if (nstc >= CPTC) {

			//si hay gente esperando, la atiendo
			var tatc = helpers.getTatc();
			tpstc[j] = t + tatc;
			sta += tatc;

		} else {

			//no hay gente esperando, me fijo si hay gente esperando en tramites largos
			if (nstl > CPTL){
				//hay gente esperando => la saco de nstl y la sumo a nstc
				nstc++;
				nstl--;
				
				//los atiendo con la fdp de tl
				var tatl = helpers.getTatl();
				tpstc[j] = t + tatl;
				sta += tatl;
			} else {
				//si no hay a quien atender => inicio tiempo ocioso
				itotc[j] = t;
				tpstc[j] = HV;
			}
		} 
	} 

	//sumo el tiempo a la sumatoria de salidas
	ss += t;
};

function procesarSalidaTramiteLargo(){
	//actualizo t con el valor de la proxima salida y resto una persona del sistema tramite largo
	t = tpstl[i];
	nstl--;

	//si hay gente en la cola fuera de turno (prioridad), se la atiende y se la suma al nstl
	if (nsft >= 1){
		nsft--;
		nstl++;

		//obtengo tatl y lo sumo para tpstl y la sumatoria de ta
		var taft = helpers.getTaft();
		tpstl[i] = t + taft;
		sta += taft;
	} else {
		//no hay gente con prioridad, pregunto si hay gente en nstl esperando
		if (nstl >= CPTL) {

			//si hay gente esperando, la atiendo
			var tatl = helpers.getTatl();
			tpstl[i] = t + tatl;
			sta += tatl;

		} else {

			//no hay gente esperando, me fijo si hay gente esperando en nstc
			if (nstc > CPTC){
				//hay gente esperando => la saco de nstc y la sumo a nstl
				nstl++;
				nstc--;
				
				//los atiendo con la fdp de tc
				var tatc = helpers.getTatc();
				tpstl[i] = t + tatc;
				sta += tatc;
			} else {
				//si no hay a quien atender => inicio tiempo ocioso
				itotl[i] = t;
				tpstl[i] = HV;
			}
		} 
	} 

	//sumo el tiempo a la sumatoria de salidas
	ss += t;
};

function procesarLlegada(){
	t = tpll;

	tpll = t + helpers.getIA();

	var r = Math.random();

	if (r > 0.4194)
		procesarLlegadaTramiteCorto();
	else if (r < 0.0893)
		procesarLlegadaFueraDeTurno();
	else
		procesarLlegadaTramiteLargo();	
};

function procesarLlegadaTramiteCorto(){
	if (helpers.arrepentimientoTC(nstc)){
		//agrego una persona a los arrepentidos de tc y listo
		ntatc++; 
		return;
	}

	//sumo uno a la cola de tc
	nstc++;
	sll += t;

	//si no hay gente en nsft (prioridad) y hay algun puesto de tc libre, me atienden
	if (nsft == 0 && nstc <= CPTC){
		var ta = helpers.getTatc();

		//busco puesto ocioso y sumo el tiempo que estuvo ocioso
		j = helpers.buscarPuestoOcioso(tpstc, HV);
		stotc[j] += t - itotc[j];

		tpstc[j] = t + ta;
		sta += ta;

	} else if (nsft == 0 && nstc > CPTC && nstl < CPTL) { //TODO: Dudas en este if, para mi debe ser al reves
		//saco gente de nstl y los paso a nstc
		nstl++;
		nstc--;

		var ta = helpers.getTatc();
		//busco puesto ocioso y sumo el tiempo que estuvo ocioso
		i = helpers.buscarPuestoOcioso(tpstl, HV);
		stotl[i] += t - itotl[i];

		tpstl[i] = t + ta;
		sta += ta;
	}

	//incremento numero total de personas que pasaron por el sistema
	nt++;
};

function procesarLlegadaTramiteLargo(){
	if (helpers.arrepentimientoTL(nstl)){
		//agrego una persona a los arrepentidos de tl y listo
		ntatl++; 
		return;
	}

	//sumo uno a la cola de tl
	nstl++;
	sll += t;

	//si no hay gente en nsft (prioridad) y hay algun puesto de tl libre, me atienden
	if (nsft == 0 && nstl <= CPTL) {
		var ta = helpers.getTatl();

		//busco puesto ocioso y sumo el tiempo que estuvo ocioso
		i = helpers.buscarPuestoOcioso(tpstl, HV);
		stotl[i] += t - itotl[i];

		tpstl[i] = t + ta;
		sta += ta;

	} else if (nsft == 0 && nstl > CPTL && nstc < CPTC) { //TODO: Dudas en este if, para mi debe ser al reves
		//saco gente de nstl y los paso a nstc
		nstc++;
		nstl--;

		var ta = helpers.getTatl();
		//busco puesto ocioso y sumo el tiempo que estuvo ocioso
		j = helpers.buscarPuestoOcioso(tpstc, HV);
		stotc[j] += t - itotc[j];

		tpstc[j] = t + ta;
		sta += ta;
	}

	//incremento numero total de personas que pasaron por el sistema
	nt++;
};

function procesarLlegadaFueraDeTurno(){
	if (helpers.arrepentimientoFT(nsft)) {
		//agrego una persona a los arrepentidos de ft y listo
		ntaft++; 
		return;
	}

	//sumo uno a la cola de ft
	nsft++;
	sll += t;

	//me fijo si hay puesto de tc libre
	if (nstc < CPTC){
		//si hay, saco de nsft para poner en nstc
		nsft--;
		nstc++;

		//busco puesto ocioso y sumo el tiempo que estuvo ocioso
		j = helpers.buscarPuestoOcioso(tpstc, HV);
		stotc[j] += t - itotc[j];

		//atiendo ft
		var ta = helpers.getTaft();
		tpstc[j] = t + ta;
		sta += ta;

	} else if (nstl < CPTL) { //me fijo si hay puesto de tl libre
		//si hay, saco de nsft para poner en nstl
		nsft--;
		nstl++;

		//busco puesto ocioso y sumo el tiempo que estuvo ocioso
		i = helpers.buscarPuestoOcioso(tpstl, HV);
		stotl[i] += t - itotl[i];

		//atiendo ft
		var ta = helpers.getTaft();
		tpstl[i] = t + ta;
		sta += ta;
	}

	//incremento numero total de personas que pasaron por el sistema
	nt++;
};

function inicializarVariables(){
	t = 0;
	tpll = 0;
	for(var h = 0; h < CPTL; h++) {
		//tpstl[h] = HV;
		//stotl[h] = 0; 
		tpstl.push(HV);
		stotl.push(0);
		ptotl.push(0);
		itotl.push(0);
	}

	for(var h = 0; h < CPTC; h++) {
		//tpstc[h] = HV;
		//stotc[h] = 0;
		tpstc.push(HV);
		stotc.push(0);
		ptotc.push(0);
		itotc.push(0);
	}

	i = 0;
	j = 0;
	ss = 0;
	sll = 0;
	sta = 0;
	nt = 0;
	ntatc = 0;
	ntatf = 0;
	ntatl = 0;
	TF = 0;
	nstl = 0;
	nstc = 0;
	nstf = 0;
};
