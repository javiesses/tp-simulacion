module.exports = function(){
	return {
		mostrarResultados: function(ptotl, ptotc, pte, nta, patc, patl, paft,cantidadSimulaciones, CPTL, CPTC){

			//promedio tiempo de espera
			pte = pte/cantidadSimulaciones;
			
			//arrepentidos
			nta = nta/cantidadSimulaciones;
			patc = patc/cantidadSimulaciones;
			patl = patl/cantidadSimulaciones;
			paft = paft/cantidadSimulaciones;

			//tiempo ocioso
			for(var h = 0; h < CPTL; h++) 
				ptotl[h] = ptotl[h]/cantidadSimulaciones;

			for(var h = 0; h < CPTC; h++) 
				ptotc[h] = ptotc[h]/cantidadSimulaciones;

			console.log('');
			console.log('-------------------------------------------------------------------------------');
			console.log('Resultados de la simulación:');
			console.log('');
			console.log('Cantidad de simulaciones: ', cantidadSimulaciones)	;
			console.log('');
			console.log('Cantidad de puestos de atención para trámites largos: ', CPTL);
			console.log('Cantidad de puestos de atención para trámites cortos: ', CPTC);
			console.log('');
			console.log('Promedio de tiempo de espera: ', pte.toFixed(2), 'minutos');
			console.log('');
			console.log('Número total de personas arrepentidas: ', nta);
			console.log('Porcentaje de arrepentidos en trámites largos: ', (patl || 0).toFixed(2),'%');
			console.log('Porcentaje de arrepentidos en trámites cortos: ', (patc || 0).toFixed(2),'%');
			console.log('Porcentaje de arrepentidos en trámites fuera de turno: ', (paft || 0)).toFixed(2),'%');
			console.log('');
			console.log('Tiempo ocioso en los puestos de atención para trámites largos');

			for(var h = 0; h < CPTL; h++) 
				console.log('Porcentaje de tiempo ocioso para el puesto de atención número ', h + 1, ': ', (ptotl[h] / cantidadSimulaciones).toFixed(2), '%');

			console.log('');
			console.log('Tiempo ocioso en los puestos de atención para trámites cortos');

			for(var h = 0; h < CPTC; h++) 
				console.log('Porcentaje de tiempo ocioso para el puesto de atención número ', h + 1, ': ', (ptotc[h] / cantidadSimulaciones).toFixed(2), '%');
			
			console.log('');
		},
		
		buscarMenor: function(arr){
			var j = 0;

			for (var i = 0; i < arr.length; i++){
				if (arr[i] <= arr[j])
					j = i;
			}

			return j;
		},

		buscarPuestoOcioso: function(arr, HV){
			for (var i = 0; i < arr.length; i++){
				if (arr[i] == HV)
					return i;
			}

			throw 'No hay puesto ocioso';
		},

		getTatl: function(){
			var gammaDist = function(x){
				var alpha = 0.83931;
				var beta = 993.74;
				var gama = 903.0;

				var numerador = (x - gama)^(alpha - 1);
				var denominador = beta^alpha * gamma(alpha);
				var exponente = -(x - gama) / beta;

				return (numerador / denominador) ^ exponente;				
			}

			return this.getValorPorRechazo(900,8000,gammaDist,true);
		},

		getTatc: function(){
			var normal = function(x){
				var tita = 3.547;
			    var mu = 8.992;

			    var denominador = tita * Math.sqrt(2 * Math.PI);
			    var fraccion = (x - mu) / Math.sqrt(2) * tita;
			    var numerador = Math.pow(Math.E, 1 * Math.pow(fraccion, 2));

			    return numerador / denominador;
			}

			return this.getValorPorRechazo(5, 9, normal);
		},

		getTaft: function(){
			var gammaDist = function(x){
				var alpha = 1.1814;
				var beta = 1279.2;
				var gama = 24.615;

				var numerador = (x - gama)^(alpha - 1);
				var denominador = beta^alpha * gamma(alpha);
				var exponente = -(x - gama) / beta;

				return (numerador / denominador) ^ exponente;				
			}

			return this.getValorPorRechazo(0, 6000, gammaDist, true);
		},

		getValorPorRechazo: function(a, b, funcion, pasarAMinutos) {
		    var r1, r2 = 1, x, fi = 0; //inicializo fi > 1 para forzar que entre en el while

		    while (r2 > fi){
				r1 = Math.random();
				r2 = Math.random();

			    x = a + (b - a)*r1,
			    fi = funcion(x);
			}


		    if (pasarAMinutos)
		    	return x/60; 

	    	return x;
		},

		arrepentimiento: function(ns){
			//el 52% se va si encuentra 8 o mas personas en la fila
			if (ns < 5)
				return false;

			return Math.random() < 0.52;
		},

		arrepentimientoTC: function(nstc){
			return this.arrepentimiento(nstc);
		},

		arrepentimientoTL: function(nstl){
			return this.arrepentimiento(nstl);
		},

		arrepentimientoFT: function(nsft){
			return this.arrepentimiento(nsft);
		},

		getIA: function(){
			return Math.random() * (10.516 - 3.734) + 3.734;
		}
	}
}
